package kz.beeline.logic;

public class Task2 {

    public static Integer getDigitsSum(Integer number){
        if (number != 0) {
            Integer digit = number % 10;
            number = number / 10;
            return digit + getDigitsSum(number);
        }
        return 0;
    }
}
