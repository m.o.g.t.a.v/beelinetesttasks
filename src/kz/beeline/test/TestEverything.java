package kz.beeline.test;

import kz.beeline.logic.Task1;
import kz.beeline.logic.Task2;
import kz.beeline.logic.Task3;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

class TestEverything {

    @Test
    public void testTask1(){
        assertEquals("234", Task1.getSum("111", "123"));
    }

    @Test
    public void testTask2(){
        assertEquals((int) 6, (int) Task2.getDigitsSum(123));
    }

    @Test
    public void testTask3ForWinHorizontal(){
        String[] gameField = new String[10];
        gameField[0] = "XX.XX.....";
        gameField[1] = ".....OOOO.";
        gameField[2] = "..........";
        gameField[3] = "..........";
        gameField[4] = "..........";
        gameField[5] = "..........";
        gameField[6] = "..........";
        gameField[7] = "..........";
        gameField[8] = "..........";
        gameField[9] = "..........";

        assertEquals("YES", Task3.canAliceWinInOneMove(gameField));
    }

    @Test
    public void testTask3ForWinVertical(){
        String[] gameField = new String[10];
        gameField[0] = "..........";
        gameField[1] = ".....OOOO.";
        gameField[2] = "..........";
        gameField[3] = "..........";
        gameField[4] = "....X.....";
        gameField[5] = "..........";
        gameField[6] = "....X.....";
        gameField[7] = "....X.....";
        gameField[8] = "....X.....";
        gameField[9] = "..........";

        assertEquals("YES", Task3.canAliceWinInOneMove(gameField));
    }

    @Test
    public void testTask3ForWinDiagonal(){
        String[] gameField = new String[10];
        gameField[0] = "..........";
        gameField[1] = ".....OOOO.";
        gameField[2] = "..........";
        gameField[3] = "..........";
        gameField[4] = "...X......";
        gameField[5] = "..........";
        gameField[6] = ".....X....";
        gameField[7] = "......X...";
        gameField[8] = ".......X..";
        gameField[9] = "..........";

        assertEquals("YES", Task3.canAliceWinInOneMove(gameField));
    }

    @Test
    public void testTask3ForLose(){
        String[] gameField = new String[10];
        gameField[0] = "XXOXX.....";
        gameField[1] = "OO.O......";
        gameField[2] = "..........";
        gameField[3] = "..........";
        gameField[4] = "..........";
        gameField[5] = "..........";
        gameField[6] = "..........";
        gameField[7] = "..........";
        gameField[8] = "..........";
        gameField[9] = "..........";

        assertEquals("NO", Task3.canAliceWinInOneMove(gameField));
    }
}
