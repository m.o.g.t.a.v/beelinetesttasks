package kz.beeline.logic;



public class Task1 {
    public static String getSum(String firstNumber, String secondNumber){
        if (checkIfNumberLegit(firstNumber) && checkIfNumberLegit(secondNumber)){

            if (firstNumber.length() > secondNumber.length())
                secondNumber = addZerosToMinimalNumber(secondNumber, firstNumber.length() - secondNumber.length());
            else if (firstNumber.length() < secondNumber.length())
                firstNumber = addZerosToMinimalNumber(firstNumber, secondNumber.length() - firstNumber.length());

            StringBuilder result = new StringBuilder();

            Integer additionalOne = 0;

            for (int i = firstNumber.length() - 1; i >= 0; i-- ){
                Integer twoDigitsSum = Byte.parseByte(Character.toString(firstNumber.charAt(i))) +
                                       Byte.parseByte(Character.toString(secondNumber.charAt(i))) +
                                       additionalOne;

                additionalOne = twoDigitsSum / 10;

                result.append(twoDigitsSum % 10);
            }

            if (additionalOne == 1)
                result.append(1);

            return result.reverse().toString();
        }
        return "Введено некорректное число";
    }

    private static String addZerosToMinimalNumber(String number, int zerosNumber){
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < zerosNumber; i++)
            sb.append("0");
        sb.append(number);
        return sb.toString();
    }

    private static Boolean checkIfNumberLegit(String number){
        return number != null && !number.isEmpty() && number.matches("^[0-9]*$");
    }
}
