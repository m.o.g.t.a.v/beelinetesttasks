package kz.beeline.logic;

public class Task3 {
    public static String canAliceWinInOneMove(String[] gameField){
        for (int i = 0; i < 10; i++){
            for (int j = 0; j < 10; j++){
                if (gameField[i].charAt(j) == 'X') {
                    if (checkHorizontal(gameField[i], j)) return "YES";
                    if (checkVertical(gameField, i, j)) return "YES";
                    if (checkDiagonal(gameField, i, j)) return "YES";
                }
            }
        }
        return "NO";
    }

    private static Boolean checkHorizontal(String row, int columnIndex){
        if (columnIndex <= 5){
            int xCounter = 0;
            int oCounter = 0;
            for (int i = columnIndex; i < columnIndex + 5; i++){
                if (row.charAt(i) == 'X')
                    xCounter++;
                if (row.charAt(i) == 'O')
                    oCounter++;
            }
            if (xCounter == 4 && oCounter == 0)
                return true;
        }
        return false;
    }

    private static Boolean checkVertical(String[] gameField, int rowIndex, int columnIndex){
        if (rowIndex <= 5){
            int xCounter = 0;
            int oCounter = 0;
            for (int i = rowIndex; i < rowIndex + 5; i++){
                if (gameField[i].charAt(columnIndex) == 'X')
                    xCounter++;
                if (gameField[i].charAt(columnIndex) == 'O')
                    oCounter++;
            }
            if (xCounter == 4 && oCounter == 0)
                return true;
        }
        return false;
    }

    private static Boolean checkDiagonal(String[] gameField, int rowIndex, int columnIndex){
        if (rowIndex <= 5 && columnIndex <= 5){
            int xCounter = 0;
            int oCounter = 0;
            for (int i = rowIndex; i < rowIndex + 5; i++)
                for (int j = columnIndex; j < columnIndex + 5; j++) {
                    if (gameField[i].charAt(j) == 'X')
                        xCounter++;
                    if (gameField[i].charAt(j) == 'O')
                        oCounter++;
                }
            if (xCounter == 4 && oCounter == 0)
                return true;
        }
        return false;
    }
}
